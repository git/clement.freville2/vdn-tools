#!/bin/bash

# Helpers
_vdn_guest_hosts() {
  local hosts=$(vdn-list)
  local user=''
  local host="$cur"
  if [[ $cur == *@* ]]; then
    user="${cur%@*}@"
    host=${cur#*@}
  fi
  COMPREPLY=($(compgen -W "$hosts" -P "$user" -- "$host"))
}

_vdn_guest_files() {
  local userhost=${cur%%?(\\):*}
  local path=${cur#*:}
  local files
  if [[ $1 == -d ]]; then
    files=$(vdn-ssh "$userhost" \
      command ls -aF1dL "$path*/" 2>/dev/null)
  else
    files=$(vdn-ssh "$userhost" \
      command ls -aF1dL "$path*" 2>/dev/null)
  fi
  COMPREPLY=($files)
}

_vdn_local_files() {
  _filedir "$*"
}

_vdn_operation() {
  local cur prev words cword
  _init_completion || return
  if [[ $cur == -* ]]; then
    COMPREPLY=($(compgen -W '-h' -- "$cur"))
  fi
}

_vdn_clean() {
  local cur prev words cword
  _init_completion || return
  if [[ $cur == -* ]]; then
    COMPREPLY=($(compgen -W '-f -h' -- "$cur"))
  else
    _vdn_guest_hosts
  fi
}

_vdn_guest_operation() {
  local cur prev words cword
  _init_completion || return
  if [[ $cur == -* ]]; then
    COMPREPLY=($(compgen -W '-h' -- "$cur"))
  else
    _vdn_guest_hosts
  fi
}

_vdn_infos() {
  local cur prev words cword
  _init_completion || return
  local guest=${words[1]}
  local n
  for ((i = 1; i <= cword; i++)); do
    if [[ ${words[i]} != -* ]]; then
      ((n++))
    fi
  done
  case $n in
    1)
      _vdn_guest_hosts
      ;;
    2)
      local vars=$(grep -E -o '^([A-Z_]+)=' $(vdn-get-network)/$guest.conf 2>/dev/null | sed 's/=$//g')
      COMPREPLY=($(compgen -W "$vars" -- "$cur"))
      ;;
  esac
}

_vdn_kill() {
  local cur prev words cword
  _init_completion || return
  if [[ $cur == -* ]]; then
    COMPREPLY=($(compgen -W '-h -q -s' -- "$cur"))
  else
    _vdn_guest_hosts
  fi
}

_vdn_rsync() {
  local cur prev words cword
  _init_completion -n : || return
  if [[ $cur == -* ]]; then
    COMP_WORDS[0]='rsync'
    _command_offset 0
    return
  fi
  if [[ $cur == *:* ]]; then
    _vdn_guest_files
    return
  fi
  _vdn_guest_hosts
  if [[ $cur != *@* ]]; then
    _vdn_local_files
  fi
}

_vdn_scripts() {
  local cur prev words cword
  _init_completion || return
  if [[ $cur == -* ]]; then
    COMPREPLY=($(compgen -W '-h -r -n -d -t' -- "$cur"))
  else
    COMPREPLY=($(cd $(vdn-get-network)/scripts && compgen -o filenames -A file -- "$cur"))
  fi
}

_vdn_set_network_dir() {
  local cur prev words cword
  _init_completion || return
  if [[ $cur == -* ]]; then
    COMPREPLY=($(compgen -W '-h' -- "$cur"))
  else
    COMPREPLY=($(compgen -W "$(vdn-list-networks)" -- "$cur"))
  fi
}

_vdn_ssh() {
  local cur prev words cword
  _init_completion || return
  if [[ $cur == -* ]] || [[ $prev == -* ]]; then
    COMP_WORDS[0]='ssh'
    _command_offset 0
    return
  fi
  _vdn_guest_hosts
}

_vdn_sshfs() {
  local cur prev words cword
  _init_completion -n : || return
  local n
  for ((i = 1; i <= cword; i++)); do
    if [[ ${words[i]} != -* ]]; then
      ((n++))
    fi
  done
  case $n in
    1)
      if [[ $cur == *:* ]]; then
        _vdn_guest_files -d
      else
        _vdn_guest_hosts
      fi
      ;;
    2)
      _vdn_local_files -d
      ;;
  esac
}

_vdn_scp() {
  local cur prev words cword
  _init_completion -n : || return
  if [[ $cur == *:* ]]; then
    _vdn_guest_files
    return
  fi
  _vdn_guest_hosts
  if [[ $cur != *@* ]]; then
    _vdn_local_files
  fi
}

_vdn_start() {
  local cur prev words cword
  _init_completion || return
  if [[ $cur == -* ]]; then
    COMPREPLY=($(compgen -W '-h -f -g -e -t -v' -- "$cur"))
  elif [[ $prev != '-v' ]]; then
    _vdn_guest_hosts
  fi
}

complete -F _vdn_guest_operation vdn-alive
complete -F _vdn_operation vdn-alives
complete -F _vdn_clean vdn-clean
complete -F _vdn_guest_operation vdn-halt
complete -F _vdn_infos vdn-infos
complete -F _vdn_kill vdn-kill
complete -F _vdn_set_network_dir vdn-open-network
complete -F _vdn_rsync vdn-rsync
complete -F _vdn_guest_operation vdn-save
complete -F _vdn_scp vdn-scp
complete -F _vdn_scripts vdn-scripts
complete -F _vdn_set_network_dir vdn-set-network-dir
complete -F _vdn_ssh vdn-ssh
complete -F _vdn_sshfs vdn-sshfs
complete -F _vdn_start vdn-start
complete -F _vdn_guest_operation vdn-viewer
