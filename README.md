# vdn-tools

A collection of scripts for Virtual Didactic Network (VDN).

Most components can be used individually without the other scripts. You will find here a script to complete the commands with Bash, and a way to use some programs such as `rsync`, `drone` or `php`.

## Bash completion script

This script can complete the most commonly used commands in VDN like `vdn-ssh`, `vdn-start` or `vdn-scp`.

It can complete network and system names. Guest filenames can also be completed like the `scp` command.

### Locally

Source the `completion/vdn-completion.bash` file in your `.bashrc`:

```bash
. ~/path/to/completions/vdn-completion.bash
```

### Globally

Copy the `vdn-completion.bash` file in `/usr/local/share/bash-completion/completions/` and link all the commands:

```bash
cp completion/vdn-completion.bash /usr/local/share/bash-completion/completions/vdn
grep 'complete -F \w* [a-z-]*' -- completion/vdn-completion.bash | awk '{ print "/usr/local/share/bash-completion/completions/"$NF }' | xargs -I {} ln -s /usr/local/share/bash-completion/completions/vdn {}
```

## `vdn-drone`

Execute your pipeline locally.

Run the pipeline named `default`:

```bash
vdn-drone exec --pipeline default
```

## `vdn-php`

Run PHP 8.2 in your current host directory. It forwards the server port and mounts the files in the VDN guest system.

Run the PHP integrated server:

```bash
vdn-php -S 0.0.0.0:8000
#vdn-php -S localhost:8000 # This will not work, the server needs to listen on all addresses!
```

On your host system, use your favorite browser and check your website at `http://localhost:8000`.

## `vdn-rsync`

Copy files between the host and the guest system using *rsync*.

### With a script

Use the `bin/vdn-rsync` script to have a nice interface with *rsync*.

```bash
vdn-rsync -av src root@bigboss:
```

Add it to your path:

```bash
PATH="$PATH:~/path/to/vdn-tools/bin/"
```

### Without a script

Specify the `vdn-ssh` command with `rsync`.

```bash
rsync -av src -e vdn-ssh root@bigboss:
```
